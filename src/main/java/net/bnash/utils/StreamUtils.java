package net.bnash.utils;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class StreamUtils<T> {

    public static <T> Stream<T> toStream(final Collection<T> collection) {
        return (collection != null) ? collection.stream() : Stream.empty();
    }

    public static <T> Stream<T> toStream(final T[] array) {
        return (array != null) ? Arrays.stream(array) : Stream.empty();
    }

    public static IntStream toStream(final int[] array) {
        return (array != null) ? Arrays.stream(array) : IntStream.empty();
    }

    public static LongStream toStream(final long[] array) {
        return (array != null) ? Arrays.stream(array) : LongStream.empty();
    }

    public static DoubleStream toStream(final double[] array) {
        return (array != null) ? Arrays.stream(array) : DoubleStream.empty();
    }
}
