package net.bnash.utils;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Stream;

import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class StreamUtilsTests {

    private StreamUtilsTests() {
        //Static interface classc
    }

    @Test
    void toStream_withNullCollection_returnEmpty() {
        final List<String> list = null;
        final Stream<String> result = StreamUtils.toStream(list);
        assertThat(result, is(notNullValue()));
        assertThat(result.count(), is(0L));
    }

    @Test
    void toStream_withCollection_returnCollectionStream() {
        final List<String> list = mock(List.class);
        final Stream<String> result = StreamUtils.toStream(list);
        assertThat(result, is(notNullValue()));
        verify(list).stream();
    }

    @Test
    void toStream_withNullObjectArray_returnEmpty() {
        final String[] array  = null;
        final Stream<String> result = StreamUtils.toStream(array);
        assertThat(result, is(notNullValue()));
        assertThat(result.count(), is(0L));
    }

    @Test
    void toStream_withObjectArray_returnCollectionStream() {
        final String value = "value";
        final String[] array  = {value};
        final Stream<String> result = StreamUtils.toStream(array);
        assertThat(result, is(notNullValue()));
        assertThat(result.findFirst().orElse(null), is(value));
    }

    @Test
    void toStream_withNullIntArray_returnEmpty() {
        final int[] array  = null;
        var result = StreamUtils.toStream(array);
        assertThat(result, is(notNullValue()));
        assertThat(result.count(), is(0L));
    }

    @Test
    void toStream_withIntArray_returnCollectionStream() {
        final int value = 42;
        final int[] array  = {value};
        var result = StreamUtils.toStream(array);
        assertThat(result, is(notNullValue()));
        assertThat(result.findFirst().orElse(-1), is(value));
    }

    @Test
    void toStream_withNullLongArray_returnEmpty() {
        final long[] array  = null;
        var result = StreamUtils.toStream(array);
        assertThat(result, is(notNullValue()));
        assertThat(result.count(), is(0L));
    }

    @Test
    void toStream_withLongArray_returnCollectionStream() {
        final long value = 42L;
        final long[] array  = {value};
        var result = StreamUtils.toStream(array);
        assertThat(result, is(notNullValue()));
        assertThat(result.findFirst().orElse(-1L), is(value));
    }

    @Test
    void toStream_withNullDoubleArray_returnEmpty() {
        final double[] array  = null;
        var result = StreamUtils.toStream(array);
        assertThat(result, is(notNullValue()));
        assertThat(result.count(), is(0L));
    }

    @Test
    void toStream_withDoubleArray_returnCollectionStream() {
        final double value = 42;
        final double[] array  = {value};
        var result = StreamUtils.toStream(array);
        assertThat(result, is(notNullValue()));
        assertThat(result.findFirst().orElse(-1), is(value));
    }

}